# set default shell
SHELL := $(shell which bash)
ENV = /usr/bin/env
# default shell options
.SHELLFLAGS = -c -e

TERRAFORM_BIN = terraform
GRAPH_DIR = graphs
format = png
# upgrade plugins on terraform init
TF_UPGRADE = false
# update terraform modules on terraform get
TF_UPDATE = false

opt =

.SILENT: ;               # no need for @
.ONESHELL: ;             # recipes execute in same shell
.NOTPARALLEL: ;          # wait for this target to finish
.EXPORT_ALL_VARIABLES: ; # send all vars to shell

########################################################################

default: help

help: ## display help for make commands
	grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.PHONY: help

all:
	$(MAKE) init
	$(MAKE) check
.PHONY: all

########################################################################
### Cleaning

clean: ## delete all generated and uploaded files
	$(MAKE) _clean_output
	$(MAKE) _clean_graphs
	$(MAKE) _clean_tf
.PHONY: clean

########################################################################
### code checking

check: lint fmt_check ## validate and lint terraform config
.PHONY: check

lint: ## lint terraform config
	$(TERRAFORM_BIN) validate
.PHONY: lint

fmt_check: ## check terraform format
	$(TERRAFORM_BIN) fmt -check=true -write=false
.PHONY: fmt_check

fmt: ## format terraform code
	$(TERRAFORM_BIN) fmt
.PHONY: fmt

########################################################################
### terraform commands

tf: ## run terraform generic command use opt="" to pass arguments
	$(TERRAFORM_BIN) $(opt)
.PHONY: tf

_get:
	$(TERRAFORM_BIN) get -update=$(TF_UPDATE)
.PHONY: _get
init: ## initialize terraform config
	$(TERRAFORM_BIN) init -get=true -upgrade=$(TF_UPGRADE) $(opt)
.PHONY: init

init_upgrade: ## initialize terraform config with plugins and providers upgrade
	$(MAKE) init TF_UPGRADE=true $(opt)
.PHONY: init_upgrade

plan: _get ## run terraform and get a plan to apply
	$(TERRAFORM_BIN) plan $(opt)
.PHONY: plan

apply: _get _clean_output ## run terraform and apply changes
	$(TERRAFORM_BIN) apply $(opt)
.PHONY: apply

destroy: _get _clean_output ## run terraform and destroy all resources
	$(TERRAFORM_BIN) destroy $(opt)
.PHONY: destroy


_clean_tf:
	rm -rf .terraform;
	rm -rf ssl;
.PHONY: _clean_tf

########################################################################
### outputs

output: ## display terraform output
	$(TERRAFORM_BIN) output $(opt)
.PHONY: output

_clean_output:
	rm -f output_raw.json output.json output.yml
.PHONY: _clean_output

output_raw.json:
	$(MAKE) output opt="-json $(opt)" > output_raw.json

output.json: output_raw.json
	jq 'map_values(.value)' output_raw.json > output.json

output_json: output.json ## display terraform output as JSON
	jq -C . output.json
.PHONY: output_json

output.yml: output.json
	yq read output.json -P > output.yml

output_yaml: output.yml ## display terraform output as YAML
	cat output.yml | pygmentize -l yaml
.PHONY: output_yaml

_clean_graphs:
	rm -rf graphs
.PHONY: _clean_graphs

_graph_dir: _get
	mkdir -p graphs
.PHONY: _graph_dir

_graph:
	$(TERRAFORM_BIN) graph -type=$(type) -draw-cycles $(opt) | dot -T$(format) -y > $(GRAPH_DIR)/infra_$(type).$(format)
.PHONY: _graph

graphs: _graph_dir ## display terraform graphs
	$(MAKE) _graph type=plan format=$(format)
	$(MAKE) _graph type=plan-destroy format=$(format)
	$(MAKE) _graph type=apply format=$(format)
	echo "Graphs exported to '$(GRAPH_DIR)' directory"
.PHONY: graphs
