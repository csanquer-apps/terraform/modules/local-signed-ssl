variable "output_path" {
  description = "Output directory path for all certificates files"
  type        = string
  default     = "."
}

variable "output_name" {
  description = "Output name"
  type        = string
  default     = "my_custom"
}

variable "output_files" {
  description = "Output to files"
  type        = bool
  default     = false
}

## Private key

variable "algorithm" {
  description = "SSL algorithm (RSA or ECDSA)."
  type        = string
  default     = "RSA"
}

variable "ecdsa_curve" {
  type        = string
  description = "ECDSA Elliptic curve (P224, P256, P384 or P521)."
  default     = "P256"
}

variable "rsa_bits" {
  description = "RSA key size in bits."
  type        = number
  default     = 2048
}

variable "private_key_pem" {
  description = "External private key provided in PEM encoded format. Leave empty to generate a new one."
  type        = string
  default     = ""
}

##
variable "ca_algorithm" {
  description = "SSL algorithm (RSA or ECDSA) from Certificate Authority."
  type        = string
  default     = "RSA"
}

variable "ca_private_key_pem" {
  description = "External private key provided in PEM encoded format from Certificate Authority."
  type        = string
}

variable "ca_cert_pem" {
  description = "Certificate Authority provided in PEM encoded format."
  type        = string
}

variable "ca_cert_bundle_pem" {
  description = "Certificate Authority bundle chain provided in PEM encoded format."
  type        = string
  default     = ""
}

## locally signed certificate

variable "is_ca" {
  description = "set if it is a certificate authority."
  type        = bool
  default     = false
}

variable "validity_period_hours" {
  description = "The number of hours after initial issuing that the certificate will become invalid."
  type        = number
  default     = 17520 # 2×365×24 = 17520 = 2 years
}

variable "allowed_uses" {
  description = "List of keywords from RFC5280 describing a use that is permitted. For more info and the list of keywords, see https://www.terraform.io/docs/providers/tls/r/self_signed_cert.html#allowed_uses."
  type        = list(string)

  default = [
    # "cert_signing",
    "key_encipherment",
    "digital_signature",
  ]
}

variable "dns_names" {
  description = "List of DNS names for which a certificate is being requested."
  type        = list(string)
  default     = []
}

variable "ip_addresses" {
  description = "List of IP addresses for which a certificate is being requested."
  type        = list(string)
  default     = []
}

variable "uris" {
  description = "List of URIs for which a certificate is being requested."
  type        = list(string)
  default     = []
}

variable "early_renewal_hours" {
  description = "If set, the resource will consider the certificate to have expired the given number of hours before its actual expiry time."
  type        = number
  default     = 0
}

variable "set_subject_key_id" {
  description = "If true, the certificate will include the subject key identifier. Defaults to false, in which case the subject key identifier is not set at all."
  type        = bool
  default     = false
}


variable "common_name" {
  description = "SSL subject Common Name = Domain."
  type        = string
}

variable "organization" {
  description = "SSL subject organization name."
  type        = string
  default     = ""
}

variable "organizational_unit" {
  description = "SSL subject organizational unit name."
  type        = string
  default     = ""
}

variable "street_addresses" {
  description = "SSL subject street addresses list."
  type        = list(string)
  default     = []
}

variable "locality" {
  description = "SSL subject locality."
  type        = string
  default     = ""
}

variable "province" {
  description = "SSL subject province."
  type        = string
  default     = ""
}

variable "country" {
  description = "SSL subject country."
  type        = string
  default     = ""
}

variable "postal_code" {
  description = "SSL subject postal code."
  type        = string
  default     = ""
}

variable "serial_number" {
  description = "SSL subject serial number."
  type        = string
  default     = ""
}
